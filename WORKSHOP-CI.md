# CI 

## Executing a Pipeline

The current project contains a `.gitlab-ci.yml`for it to be executed by the Gitlab CI.  
As the current project is not open to contribution, you need to fork it as one of your personal project.

This is the same Java project as the one from past week with Test cases been added.

Your task is to enable the execution of the pipeline on Gitlab CI with a shared runner.  
Currently, only the compile task `assemble` is set up. You will add a new *test* step (almost like the `*build* step), push it as new commit in your repository and see been executed.

The gradle test phase is `check`. You could use the `build` phase but that adds the unecessary `package` steps and spoil precious resources from the runners that may have other pipeline to execute.

You will also need to change the policy to *pull*  as this step will rely on the previous one where a *push* policy is specified.

## Publish Test Reports

If you check the execution of a test phase, you see that test file reports are stored locally on the container instance that has executed the phase.  
You need to specify explicitely to the CI that certain files need to be uploaded to the main server.o

Junit test report are kind of standard and Gitlab is able to process them without adding any configuration.

Add the following content to your *test* step in your `.gitlab-ci.yml` at the same level than the `script` level.

```
  artifacts:
    when: always
    reports:
      junit: build/test-results/test/**/TEST-*.xml
```
